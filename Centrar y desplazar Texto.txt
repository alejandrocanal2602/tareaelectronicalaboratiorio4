#include <LiquidCrystal.h>
char texto[] = "Hola Mundo";
int offset  = sizeof(texto)/2;
int time = 200;

LiquidCrystal lcd(12,11,9,8,7,6,5,4,3,2);

void setup() 
{  
  lcd.begin(16,2);
  lcd.cursor();
  for (int i = 0; i <8 - offset; i++) 
  {
    lcd.scrollDisplayRight(); 
  }
  lcd.print(texto);
}

void loop() 
{
  for(int i = 0; i < 4 ; i++)
  {
    lcd.scrollDisplayRight(); 
    delay(time);
  }
  for(int i = 0; i < 4 ; i++)
  {
    lcd.scrollDisplayLeft(); 
    delay(time);
  }
  for(int i = 0; i < 4 ; i++)
  {
    lcd.scrollDisplayLeft(); 
	delay(time);  
  }
  for(int i = 0; i < 4 ; i++)
  {
    lcd.scrollDisplayRight(); 
  	delay(time);
  }
}